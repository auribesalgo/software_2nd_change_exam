package mondragon.edu;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Unit test for simple App.
 */
@RunWith(value = Parameterized.class)
public class ParamentersTest {
    
    exam2 exam2;
    int n;

    @Before
    public void setUp() {
        exam2 = new exam2();
    }

    @After
    public void tearDown() {
        exam2 = null;
    }

    @Parameters
    public static Collection<Object[]> numbers() {
        return Arrays.asList(new Object[][] { { 0 }, { 1 } });
    }

    public ParamentersTest(int n) {
        this.n = n;
    }

    @Test
    public void testExam() {
        assertTrue(exam2.exam(n));
    }
}
